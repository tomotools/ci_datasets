# ci_datasets

project to store / retrieve dataset for tomotools CI like https://gitlab.esrf.fr/tomo/tomo_test_data

Each project of the tomotools have its own branch.
For example the datasets used by tomoscan are on the 'tomoscan' branch.

uses [LFS storage](https://docs.gitlab.com/ee/topics/git/lfs/)

## how to add new files ?

1. git clone the project

2. checkout on the branch you want to add dataset to

For example to add project for 'tomoscan' project you must be in the 'tomoscan' branch

``` bash
git checkout tomoscan
```

3. Add your files to a new folder

4. If necessary add some extension to be considered as 'lfs'

``` bash
git lfs track "*.iso"
```

5. call git lfs files

``` bash
git lfs ls-files
```

And check the targetted files are listed.

5. create another branch, commit and create a PR to the project.

``` bash
git checkout -b ...
git commit
git push ...
```

*!warning!: the PR should target the 'dedicated' branch of the project.

